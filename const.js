const BOOKINGS = 'BOOKINGS';
const CARTYPE = 'CARTYPE';

const RATE = {
    Economy: {
        base: 5,
        rate: 1
    },
    Luxury: {
        base: 8,
        rate: 1.8
    },
    SUV: {
        base: 7,
        rate: 1.2
    },
    Mpv: {
        base: 7,
        rate: 1.2
    }
}